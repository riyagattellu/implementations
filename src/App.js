import logo from './logo.svg';
import './App.css';
import React, {useState} from 'react'

function App() {
  const [count, setCount] = useState(0);
  console.log("count", count);
  return (
    <div className="App">
      <header className="App-header">
        <h2>Counter : {count}</h2>
        <div style={{display:'flex'}}>
        <button onClick = {()=>setCount(count+1)}>Increment</button>
        <button onClick = {()=>setCount(count-1)}>Decrement</button>
        </div>
      </header>
    </div>
  );
}

export default App;
